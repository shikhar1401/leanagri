import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DataService } from "../data.service";
import { CommentStmt } from "@angular/compiler";

@Component({
  selector: "app-viewpost",
  templateUrl: "./viewpost.component.html",
  styleUrls: ["./viewpost.component.css"]
})
export class ViewpostComponent implements OnInit {
  comments = [];
  data;
  id: string;
  constructor(private route: ActivatedRoute, private dataService: DataService) {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });

    this.dataService.getPostComment(this.id).then((res: any[]) => {
      console.log(res);
      this.comments = res;
    });

    this.dataService.getPost(this.id).subscribe(res => {
      console.log(res);
      this.data = res;
      console.log(this.data);
    });
  }
  ngOnInit() {}
}
