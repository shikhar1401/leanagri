import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs";
import { ToastrService } from 'ngx-toastr';

import { DataService } from "../data.service";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.css"]
})
export class PostComponent implements OnInit {
  post = {
    userId: "",
    title: "",
    body: "",
    id: ""
  };
  update: any = {
    userId: "",
    title: "",
    body: "",
    id: ""
  };
  id: string;
  newpost: boolean = false;
  updatepost : boolean =false;
  button: boolean = true;
  posts = [];
  test = this.posts;
  constructor(
    private dataService: DataService,
    private toastr: ToastrService
    ) {

    this.dataService.get_posts().then((res: any[]) => {
      this.posts = res;
      console.log(this.posts);
    });
  }
  ngOnInit() {}
  newPost() {
    this.newpost = true;
  }
  add() {
    
    this.toastr.success('At the end!','Post added!');
    this.newpost  = false;
    let obj = {
      title: this.post.title,
      body: this.post.body,
      userId: this.post.userId
    };

    this.dataService.createPost(obj).then(res => {
      this.posts.push(res);
    });
    this.post.id =" ";
    this.post.body = " ";
    this.post.title = " ";
    this.post.userId = " ";
  }
  editPost(id){
    this.updatepost = true;
    this.button = false;
    this.id = id;

    this.dataService.getPost(this.id).subscribe((res)=>{
      this.update= res;
    })
  }
  updatePost(){
    
    this.toastr.success('check the post for the changes!','Post updated!');
    this.button = true;
    this.updatepost = false;
    let obj = {
      title: this.update.title,
      body: this.update.body,
      userId: this.update.userId
    }
    let newindex = this.posts.findIndex(item => item.id ==this.id) ;
    this.posts[newindex] = obj;
  }
}
