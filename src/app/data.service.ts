import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class DataService {
  baseUrl: string = "https://jsonplaceholder.typicode.com/posts";
  // posts: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  constructor(private http: HttpClient) {}

  get_posts(): Promise<any> {
    return new Promise(resolve => {
      this.http.get(this.baseUrl).subscribe((res: any[]) => {
        // this.posts.next(res);
        resolve(res);
      });
    });
  }

  getPostComment(id): Promise<any> {
    return new Promise(resolve => {
      this.http
        .get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
        .subscribe((res: any[]) => {
          const arr = res.filter(item => item.postId == id);
          resolve(arr);
        });
    });
  }

  getPost(id): Observable<Object> {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }

  createPost(data): Promise<any> {
    return new Promise(resolve => {
      this.http
        .post("https://jsonplaceholder.typicode.com/posts", data)
        .subscribe(res => {
          resolve(res);
        });
    });
  }
}
