import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostComponent } from './post/post.component';
import { ViewpostComponent } from './viewpost/viewpost.component';
 
const routes: Routes = [
  { path: '', redirectTo: '/post', pathMatch: 'full' },
  { path: 'post', component:PostComponent},
  { path: 'post/:id', component:ViewpostComponent}
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}